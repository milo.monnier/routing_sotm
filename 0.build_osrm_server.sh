# Build  OSRM
sudo apt update
sudo apt install -y git cmake build-essential jq liblua5.2-dev libboost-all-dev libprotobuf-dev libtbb-dev libstxxl-dev libbz2-dev cmake
# No problem on Debian stretch. But libboost problem on Ubuntu16.04
# --> Use aptitude to downgrade libboost packages

# To use in R
sudo apt install -y libcurl4-openssl-dev libgeos-dev

mkdir -p ~/Applications
cd ~/Applications
git clone https://github.com/Project-OSRM/osrm-backend.git
cd osrm-backend/
mkdir build
cd build/
cmake ..

# Make it. Long ... use multiprocessing
make -j$(nproc)
sudo make install

cd ~/Applications/osrm-backend

# Create a swapfile
fallocate -l 100G swapfile
chmod 600 swapfile
mkswap swapfile
sudo swapon swapfile

# download OSM road network data and extract it
file='france-latest'
wget -nc http://download.geofabrik.de/europe/${file}.osm.pbf

osrm-extract ${file}.osm.pbf -p profiles/car.lua
osrm-contract ${file}.osrm

# Check if server if running
osrm-routed ${file}.osrm