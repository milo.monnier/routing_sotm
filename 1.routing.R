library(sf)
library(osrm)
library(stplanr)

rm(list = ls())
setwd("~/Documents/SOTM/")

# Launch in a separate shell
# osrm-routed --max-trip-size 1000000 ~/Applications/osrm-backend/france-latest.osrm

# Set local machine as OSRM server
options(osrm.server = "http://127.0.0.1:5000/")

# Load data
load("data/Network.RData")

# Tomatoe only
dots = dots[!is.na(match(dots$tomate, 1)), ]
link = link[!is.na(match(link$produit, "tomate")), ]

# Drop geometry to extract coordinates
plot(st_geometry(link))
link.df = st_drop_geometry(link)
nrow(link.df)

# Set origin and destination coordinates
src = link.df[,c("sireti", "lngi", "lati")]
dst = link.df[,c("siretj", "lngj", "latj")]
head(src)
head(dst)

# Compute road trips
roads = NULL
for (i in 1:nrow(link)) {
  trip = osrmRoute(src = src[i,], dst = dst[i,], overview = "full", returnclass = "sf")
  roads = rbind(roads, trip)
}
class(roads)
nrow(roads)
head(roads)

# Add new routed geometry to previous link table
link.roads = bind_cols(link.df, roads)
link.roads = st_sf(link.roads)
plot(st_geometry(link.roads))


# Aggregate flows by road sections
link.roads$n = 1
link.roads.agg = stplanr::overline2(link.roads, attrib = "n")  
class(link.roads.agg)
nrow(link.roads.agg)
head(link.roads.agg)

plot(st_geometry(link.roads.agg), lwd=link.roads.agg$n)
dev.off()

save(dots, link, link.roads.agg, file = "data/Network_routed.RData")
# save(dots, link, link.roads, link.roads.agg, file = "data/Network_routed.RData")

# Kill OSRM server
# system("kill $(ps -x | grep osrm | grep -v grep | awk '{print $1;}')")
